module gitlab.com/ydkn/bbolt-simple

go 1.13

require (
	go.etcd.io/bbolt v1.3.3
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
)
