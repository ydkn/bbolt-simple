package bboltsimple

import (
	"testing"

	"fmt"
	"math/rand"
	"time"
)

type entry struct {
	Key1 string `json:"key1"`
	Key2 int    `json:"key2"`
	Key3 bool   `json:"key3"`
}

func testNewDB() (*DB, error) {
	rand.Seed(time.Now().UnixNano())

	return New(fmt.Sprintf("/tmp/%d", rand.Int()), "foobar")
}

func TestSetValue(t *testing.T) {
	db, _ := testNewDB()

	var e entry

	err := db.Get("foo", &e)
	if err == nil {
		t.Errorf(`Get(...) returned no error`)
	}

	err = db.Set("foo", &entry{Key1: "bar", Key2: 1, Key3: true})
	if err != nil {
		t.Errorf(`Set(...) returned error: %s`, err.Error())
	}

	err = db.Get("foo", &e)
	if err != nil {
		t.Errorf(`Get(...) returned error: %s`, err.Error())
	}
	if e.Key1 != "bar" {
		t.Errorf(`Get(...) did not return stored value = %s; want %s`, e.Key1, "bar")
	}
	if e.Key2 != 1 {
		t.Errorf(`Get(...) did not return stored value = %d; want %d`, e.Key2, 1)
	}
	if e.Key3 != true {
		t.Errorf(`Get(...) did not return stored value = %v; want %v`, e.Key3, true)
	}
}

func TestPrefix(t *testing.T) {
	db, _ := testNewDB()

	_ = db.Set("key.0", &entry{Key1: "foo", Key2: 0, Key3: true})
	_ = db.Set("key.1", &entry{Key1: "bar", Key2: 1, Key3: false})
	_ = db.Set("key.2", &entry{Key1: "abc", Key2: 2, Key3: false})
	_ = db.Set("xyz.3", &entry{Key1: "xyz", Key2: 3, Key3: false})

	_ = db.Delete("key.2")

	cnt := 0

	err := db.Prefix("key.", &entry{}, func(key string, value interface{}) error {
		_ = value.(*entry)

		cnt++

		return nil
	})
	if err != nil {
		t.Errorf(`Prefix(...) returned error: %s`, err.Error())
	}
	if cnt != 2 {
		t.Errorf(`Prefix(...) did not iterate through expected count of entries = %d; want %d`, cnt, 2)
	}
}
