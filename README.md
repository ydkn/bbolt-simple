# Simple bbolt

Provides a simpler interface for the key-value store bbolt (https://github.com/etcd-io/bbolt).

## Usage

```go
db, _ := bboltsimple.New("/tmp/db")

var value1 myStruct
var value2 myStruct

_ = db.Set("foo", &value1)

_ = db.Get("foo", &value2)

_ = db.Prefix("myPrefix", &myStruct{}, func(key string, value interface{}) error {
	castValue := value.(*myStruct)

  return nil
})
```
