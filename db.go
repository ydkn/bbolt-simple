package bboltsimple

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"go.etcd.io/bbolt"
)

const (
	defaultTimeout = 1 * time.Second
)

// DB database
type DB struct {
	closed     bool
	bucketName string
	db         *bbolt.DB
}

// New creates a new database reference
func New(path string, bucketName string) (*DB, error) {
	bboltDB, err := bbolt.Open(path, 0600, &bbolt.Options{Timeout: defaultTimeout})
	if err != nil {
		return nil, err
	}

	db := DB{closed: false, bucketName: bucketName, db: bboltDB}

	return &db, nil
}

// Close closes a database
func (db *DB) Close() error {
	db.closed = true

	return db.db.Close()
}

// Exist check if a specific key exists
func (db *DB) Exist(key string) bool {
	err := db.db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(db.bucketName))
		if b == nil {
			return fmt.Errorf("bucket does not exist")
		}

		val := b.Get([]byte(key))
		if val == nil {
			return fmt.Errorf("key does not exist")
		}

		return nil
	})

	return err == nil
}

// Get load entry for given key
func (db *DB) Get(key string, value interface{}) error {
	return db.db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(db.bucketName))
		if b == nil {
			return fmt.Errorf("bucket does not exist")
		}

		val := b.Get([]byte(key))
		if val == nil {
			return fmt.Errorf("key does not exist")
		}

		return json.Unmarshal(val, value)
	})
}

// Set entry for given key
func (db *DB) Set(key string, value interface{}) error {
	bytes, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return db.db.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(db.bucketName))
		if err != nil {
			return err
		} else if b == nil {
			return fmt.Errorf("bucket does not exist")
		}

		return b.Put([]byte(key), bytes)
	})
}

// Delete removes a key from the db
func (db *DB) Delete(key string) error {
	return db.db.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(db.bucketName))
		if err != nil {
			return err
		} else if b == nil {
			return fmt.Errorf("bucket does not exist")
		}

		return b.Delete([]byte(key))
	})
}

// Prefix get all entries with given prefix
func (db *DB) Prefix(prefix string, resultType interface{}, fn func(key string, value interface{}) error) error {
	bytePrefix := []byte(prefix)
	valueType := reflect.TypeOf(resultType).Elem()

	return db.db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(db.bucketName))
		if b == nil {
			return fmt.Errorf("bucket does not exist")
		}

		c := b.Cursor()

		for k, v := c.Seek(bytePrefix); k != nil && bytes.HasPrefix(k, bytePrefix); k, v = c.Next() {
			value := reflect.New(valueType).Interface()

			err := json.Unmarshal(v, value)
			if err != nil {
				return err
			}

			err = fn(string(k), value)
			if err != nil {
				return err
			}
		}

		return nil
	})
}
